package com.konpach.qapitaltesttask.presentation.screen.goals

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.konpach.qapitaltesttask.data.repository.GoalRepositoryImpl
import com.konpach.qapitaltesttask.dataSource.remote.GoalRemoteDataSourceImpl
import com.konpach.qapitaltesttask.domain.usecase.GetGoalsUseCase
import com.konpach.qapitaltesttask.modules.NetworkModule
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GoalsViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val goals = MutableLiveData<List<GoalItem>>()
    private val useCase: GetGoalsUseCase =
        GetGoalsUseCase(GoalRepositoryImpl(GoalRemoteDataSourceImpl(NetworkModule.api)))

    fun getGoals() = goals

    init {
        compositeDisposable.add(
            useCase.getGoals()
                .subscribeOn(Schedulers.io())
                .subscribe({
                    goals.postValue(it.map { it.mapToPresentation() })
                }, {
                    throw it
                })
        )
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

}
