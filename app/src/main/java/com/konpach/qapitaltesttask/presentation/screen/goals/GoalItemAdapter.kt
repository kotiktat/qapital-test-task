package com.konpach.qapitaltesttask.presentation.screen.goals

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.konpach.qapitaltesttask.BR
import com.konpach.qapitaltesttask.R

class GoalItemAdapter(val onCLickListener: (GoalItem) -> Unit) :
    androidx.recyclerview.widget.RecyclerView.Adapter<GoalItemAdapter.GoalItemViewHolder>() {

    private var data: List<GoalItem> = listOfNotNull()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoalItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.goal_item_row, parent, false)
        return GoalItemViewHolder(binding)
    }

    fun setItems(dataList: List<GoalItem>) {
        data = dataList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: GoalItemViewHolder, position: Int) {
        holder.bind(ItemViewModel(data[position]), onCLickListener)
    }

    class ItemViewModel(item: GoalItem) {
        val mItem = item
        val name = item.name
        val targetAmount = item.targetAmount
        val currentBalance = item.currentBalance
        val goalImageURL = item.goalImageURL
    }

    class GoalItemViewHolder(val binding: ViewDataBinding) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ItemViewModel, onCLickListener: (GoalItem) -> Unit) {
            binding.setVariable(BR.data, data)
            binding.executePendingBindings()
            binding.root.setOnClickListener {
                onCLickListener(data.mItem)
            }
        }
    }
}