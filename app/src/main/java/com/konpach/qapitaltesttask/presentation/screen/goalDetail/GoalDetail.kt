package com.konpach.qapitaltesttask.presentation.screen.goalDetail

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs

import com.konpach.qapitaltesttask.R

class GoalDetail : DialogFragment() {

    private lateinit var viewModel: GoalDetailViewModel

    private val args: GoalDetailArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this)
            .get(GoalDetailViewModel::class.java)
        viewModel.setGoalId(args.goalId)
        val root = inflater.inflate(R.layout.goal_detail_fragment, container, false)
        viewModel.getFeed().observe(viewLifecycleOwner, Observer {
            root.findViewById<TextView>(R.id.dummyText).text = it.toString()
        })

        return root
    }


}
