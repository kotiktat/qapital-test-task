package com.konpach.qapitaltesttask.presentation.screen.goals

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.konpach.qapitaltesttask.R
import com.konpach.qapitaltesttask.databinding.GoalsFragmentBinding

class GoalsFragment : DialogFragment() {

    private lateinit var viewModel: GoalsViewModel

    private val goalsAdapter = GoalItemAdapter {
        findNavController().navigate(GoalsFragmentDirections.actionGoalsFragmentToGoalDetail(it.id))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(GoalsViewModel::class.java).apply { }
        viewModel.getGoals().observe(viewLifecycleOwner, Observer {
            goalsAdapter.setItems(it)
        })
        DataBindingUtil.inflate<GoalsFragmentBinding>(inflater, R.layout.goals_fragment, container, false).apply {
            lifecycleOwner = this@GoalsFragment
            data = viewModel

            goalsRecyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(
                    DividerItemDecoration(
                        context,
                        DividerItemDecoration.VERTICAL
                    )
                )
                adapter = goalsAdapter
            }
            return root
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

}
