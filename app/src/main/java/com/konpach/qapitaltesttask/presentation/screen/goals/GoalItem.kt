package com.konpach.qapitaltesttask.presentation.screen.goals

import com.konpach.qapitaltesttask.domain.model.Goal

data class GoalItem(
    val id: Int,
    val goalImageURL: String,
    val targetAmount: Float?,
    val name: String,
    val currentBalance: Float
)

fun Goal.mapToPresentation(): GoalItem = GoalItem(id, goalImageURL, targetAmount, name, currentBalance)