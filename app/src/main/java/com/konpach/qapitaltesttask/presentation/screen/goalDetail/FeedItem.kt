package com.konpach.qapitaltesttask.presentation.screen.goalDetail

import com.konpach.qapitaltesttask.domain.model.Feed

data class FeedItem(
    val id: String,
    val type: String,
    val timestamp: String,
    val message: String,
    val amount: Float,
    val userId: String,
    val savingsRuleId: Int?
)

fun Feed.mapToPresentation(): FeedItem = FeedItem(id, type, timestamp, message, amount, userId, savingsRuleId)