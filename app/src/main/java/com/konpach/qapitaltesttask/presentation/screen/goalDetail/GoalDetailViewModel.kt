package com.konpach.qapitaltesttask.presentation.screen.goalDetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.konpach.qapitaltesttask.data.repository.FeedRepositoryImpl
import com.konpach.qapitaltesttask.data.repository.RuleRepositoryImpl
import com.konpach.qapitaltesttask.dataSource.remote.FeedRemoteDataSourceImpl
import com.konpach.qapitaltesttask.dataSource.remote.RuleRemoteDataSourceImpl
import com.konpach.qapitaltesttask.domain.usecase.GetFeedUseCase
import com.konpach.qapitaltesttask.modules.NetworkModule
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GoalDetailViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val feed = MutableLiveData<List<FeedItem>>()

    private val useCase: GetFeedUseCase =
        GetFeedUseCase(
            FeedRepositoryImpl(FeedRemoteDataSourceImpl(NetworkModule.api))
        )

    fun getFeed() = feed

    fun setGoalId(goadId: Int) {
        compositeDisposable.add(
            useCase.get(goadId)
                .subscribeOn(Schedulers.io())
                .subscribe({
                    feed.postValue(it.map {
                        it.mapToPresentation()
                    })
                }, {
                    throw it
                })
        )
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}
