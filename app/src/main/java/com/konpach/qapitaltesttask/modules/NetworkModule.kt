package com.konpach.qapitaltesttask.modules

import com.konpach.qapitaltesttask.BuildConfig
import com.konpach.qapitaltesttask.dataSource.remote.api.API
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

object NetworkModule {
    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .client(OkHttpClient.Builder().build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl(BuildConfig.BASE_REST_API_URL)
            .build()
    }

    val api: API by lazy {
        retrofit.create(API::class.java)
    }
}