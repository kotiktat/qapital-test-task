package com.konpach.qapitaltesttask.dataSource.remote.api

import com.konpach.qapitaltesttask.domain.model.Feed
import com.squareup.moshi.Json

data class FeedResult(@field:Json(name = "feed") val feeds: List<Feed>)