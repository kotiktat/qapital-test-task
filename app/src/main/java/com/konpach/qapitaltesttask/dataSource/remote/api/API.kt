package com.konpach.qapitaltesttask.dataSource.remote.api

import com.konpach.qapitaltesttask.domain.model.Rule
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header

interface API {

    @GET("savingsgoals")
    fun getGoals(): Single<GoalsResult>

    @GET("savingsrules")
    fun getRules(): Single<List<Rule>>

    @GET("savingsgoals/{id}/feed")
    fun getFeed(@Header("id") id: Int): Single<FeedResult>
}