package com.konpach.qapitaltesttask.dataSource.remote

import com.konpach.qapitaltesttask.data.datasource.FeedRemoteDataSource
import com.konpach.qapitaltesttask.dataSource.remote.api.API

class FeedRemoteDataSourceImpl(private val api: API) : FeedRemoteDataSource {

    override fun get(goalId: Int) = api.getFeed(goalId).map { result ->
        result.feeds
    }

}