package com.konpach.qapitaltesttask.dataSource.remote.api

import com.konpach.qapitaltesttask.domain.model.Goal
import com.squareup.moshi.Json

data class GoalsResult(@field:Json(name = "savingsGoals") val goals: List<Goal>)