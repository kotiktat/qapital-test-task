package com.konpach.qapitaltesttask.dataSource.remote

import com.konpach.qapitaltesttask.data.datasource.GoalRemoteDataSource
import com.konpach.qapitaltesttask.dataSource.remote.api.API

class GoalRemoteDataSourceImpl(private val api: API) : GoalRemoteDataSource {

    override fun get() = api.getGoals().map {
        it.goals
    }
}

