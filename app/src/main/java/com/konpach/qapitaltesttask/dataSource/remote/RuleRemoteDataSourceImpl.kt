package com.konpach.qapitaltesttask.dataSource.remote

import com.konpach.qapitaltesttask.data.datasource.RuleRemoteDataSource
import com.konpach.qapitaltesttask.dataSource.remote.api.API

class RuleRemoteDataSourceImpl(private val api: API) : RuleRemoteDataSource {

    override fun get() = api.getRules()
}