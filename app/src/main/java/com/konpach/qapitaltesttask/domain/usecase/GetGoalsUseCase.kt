package com.konpach.qapitaltesttask.domain.usecase

import com.konpach.qapitaltesttask.domain.repository.GoalRepository

class GetGoalsUseCase(private val goalRepository: GoalRepository) {

    fun getGoals() = goalRepository.get()
}