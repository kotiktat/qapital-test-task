package com.konpach.qapitaltesttask.domain.repository

import com.konpach.qapitaltesttask.domain.model.Goal
import io.reactivex.Single

interface GoalRepository {

    fun get(): Single<List<Goal>>
}