package com.konpach.qapitaltesttask.domain.model

data class Feed(
    val id: String,
    val type: String,
    val timestamp: String,
    val message: String,
    val amount: Float,
    val userId: String,
    val savingsRuleId: Int?
)