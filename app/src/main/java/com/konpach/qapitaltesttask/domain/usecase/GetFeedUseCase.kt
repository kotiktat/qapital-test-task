package com.konpach.qapitaltesttask.domain.usecase

import com.konpach.qapitaltesttask.domain.repository.FeedRepository

class GetFeedUseCase(private val feedUseCase: FeedRepository) {

    fun get(goalId: Int) =
        feedUseCase.get(goalId)

}