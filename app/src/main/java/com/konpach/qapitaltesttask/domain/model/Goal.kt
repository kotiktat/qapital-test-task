package com.konpach.qapitaltesttask.domain.model

data class Goal(
    val id: Int,
    val goalImageURL: String,
    val userId: Int,
    val targetAmount: Float?,
    val currentBalance: Float,
    val status: String,
    val name: String,
    val connectedUsers: List<Int>?
)