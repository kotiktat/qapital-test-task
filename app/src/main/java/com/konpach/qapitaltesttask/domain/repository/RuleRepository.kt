package com.konpach.qapitaltesttask.domain.repository

import com.konpach.qapitaltesttask.domain.model.Rule
import io.reactivex.Single

interface RuleRepository {

    fun get(): Single<List<Rule>>
}