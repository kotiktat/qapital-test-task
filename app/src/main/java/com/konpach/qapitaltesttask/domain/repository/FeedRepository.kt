package com.konpach.qapitaltesttask.domain.repository

import com.konpach.qapitaltesttask.domain.model.Feed
import io.reactivex.Single

interface FeedRepository {

    fun get(goalId: Int): Single<List<Feed>>
}