package com.konpach.qapitaltesttask.domain.model

data class Rule(val id: Int, val type: String, val amount: Float)