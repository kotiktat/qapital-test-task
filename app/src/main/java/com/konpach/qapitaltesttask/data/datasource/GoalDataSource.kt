package com.konpach.qapitaltesttask.data.datasource

import com.konpach.qapitaltesttask.domain.model.Goal
import io.reactivex.Single

interface GoalRemoteDataSource{

    fun get(): Single<List<Goal>>
}

