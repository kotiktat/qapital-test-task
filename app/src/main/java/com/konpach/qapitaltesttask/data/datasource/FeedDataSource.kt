package com.konpach.qapitaltesttask.data.datasource

import com.konpach.qapitaltesttask.domain.model.Feed
import io.reactivex.Single

interface FeedRemoteDataSource {

    fun get(goalId: Int): Single<List<Feed>>
}