package com.konpach.qapitaltesttask.data.datasource

import com.konpach.qapitaltesttask.domain.model.Rule
import io.reactivex.Single

interface RuleRemoteDataSource {

    fun get(): Single<List<Rule>>
}

