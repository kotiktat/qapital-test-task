package com.konpach.qapitaltesttask.data.repository

import com.konpach.qapitaltesttask.data.datasource.RuleRemoteDataSource
import com.konpach.qapitaltesttask.domain.repository.RuleRepository

class RuleRepositoryImpl(private val ruleRemoteDataSource: RuleRemoteDataSource) : RuleRepository {

    override fun get() = ruleRemoteDataSource.get()

}