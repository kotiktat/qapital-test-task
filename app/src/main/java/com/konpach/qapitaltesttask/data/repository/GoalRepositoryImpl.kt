package com.konpach.qapitaltesttask.data.repository

import com.konpach.qapitaltesttask.data.datasource.GoalRemoteDataSource
import com.konpach.qapitaltesttask.domain.repository.GoalRepository

class GoalRepositoryImpl(private val goalRemoteDataSource: GoalRemoteDataSource) : GoalRepository {

    override fun get() = goalRemoteDataSource.get()

}