package com.konpach.qapitaltesttask.data.repository

import com.konpach.qapitaltesttask.data.datasource.FeedRemoteDataSource
import com.konpach.qapitaltesttask.domain.repository.FeedRepository

class FeedRepositoryImpl(private val feedRemoteDataSource: FeedRemoteDataSource) : FeedRepository {

    override fun get(goalId: Int) = feedRemoteDataSource.get(goalId)

}