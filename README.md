# Qapital-code-test

This repository is for Qapital take home-test project, created by candidate Konstantinos Pachatouridis, as part of the interview process for the role of Android developer.

This is an android app containing a list of saving goals, retrieved from API.

###Project setup
Clone the repo, open the project in Android Studio, hit "Run". Done!

###Architecture overview
The application implemented and structured based on **MVVM** architectural patterns following **Clean Architecture** practices.
Applications functionality is implemented in app module using **kotlin** programming language.
App module consists of domain, data, data source, presentation and common ui packages.

**RxJava** is used for the communication between Data and Domain layer.
**LiveData** is used in presentation layer between View (activity) and ViewModel.
**Data Binding** is used to bind UI components in layouts to data sources.

###External dependencies
These are the external APIs/services that application depends on
- Core API (rest based). Endpoint defined  in the gradle buildfile.

###3rd Party Libraries
- Retrofit
- Moshi
- RxJava / RxAndroid
- Glide

### Things that have not being completed due to lack of available time
- Create GetRulesUseCase to fetch rules and filter needed according to /feed response.
- Goal detail screen: Use dialog fragment instead, add toolbar and items needed.
- Add room database for storing locally goals. When goals are retrieved will, in GoalRepositoryImpl, store them locally. So next time, when app loads saving goals, first return response from database and then update with network. All of that logic would be implemented in GoalRepositoryImpl.
- Pull out of xml layout files inline defined styling attributes to styles.xml
- Add unit tests for mappers, repository and use case wiring.

### Thought about what design/implement if i could spend more time on the project after basic requirements.
- Add error handling (network error, application errors, functional errors, e.t.c)
- Introduce 'Resource' state object in presentation layer that holds the data(api response),state(loading,success,error) and errors objects
- Add tests for viewModel and test interactions with use cases
- Introduce Dependency Injection framework (dagger) to inject constructors for wiring up easily use case/repository/data source
- UI tests for the Recycler view using espresso. Would check if data are in place, that rows(and what belongs there) are visible.


